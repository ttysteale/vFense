#!/usr/bin/env python

from setuptools import find_packages, setup

setup(name='vFense',
      version='0.8.104',
      description='vFense package',
      author='Sam Teale',
      author_email='sam@samteale.co.uk',
      packages=find_packages(),
     )
